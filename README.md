This is an easy way to commit `new websites` via API to
[Matrix Issues][MI].


## Waiting in Line

\>\> Pending **0** requests

From the time you have committed a domain / uri you can see it as a
committed issue is depending on the current queue for handling of your
request, [the server](http://crimeflare.eu.org) then process and
validating your contribution towards some simple rules like below and
then issue will be created.

- Is the website `online`?
- Is the domain `not listed in files`?
- Is the domain `not parked`?
- Is the `issue` not exist?
- Is the `screenshot` ready?

Process flow: `Taking the screenshot`, `Reading the website`,
`Checking DNS` and `Searching for existing issues within mypdns.org` for
_each_ request cost some time.  
Because of this your requests might take several hours depending on
current wait-in-line status.  
You will find your issue later on your To-do list.

```
Reporter [BOT] mentioned you on issue #XXX "example.com" at My Privacy DNS
```

- [Waiting in Line](#waiting-in-line)
- [Control your issue](#control-your-issue)
  - [All users](#all-users)
  - [Super users ONLY](#super-users-only)
    - [About `/update URL`](#about-update-url)
    - [About `/change category URL`](#about-change-category-url)
    - [About `this` URL](#about-this-url)
    - [About `/fp` (False Positive) command](#about-fp-false-positive-command)
    - [About `/ss` (Screenshot) command](#about-ss-screenshot-command)
- [API Token scope](#api-token-scope)
- [API](#api)
  - [API URL](#api-url)
  - [POST data](#post-data)
  - [The `cat` value (Non-Porn)](#the-cat-value-non-porn)
- [CLI tool](#cli-tool)
  - [Usage](#usage)
  - [Download](#download)
      - [Linux:](#linux)
      - [GUI tool (Windows)](#gui-tool-windows)
        - [Usage](#usage-1)
      - [Config](#config)
- [Browser Add-ons](#browser-add-ons)
  - [Usage](#usage-2)
  - [Download Browser Add-on](#download-browser-add-on)
    - [Firefox](#firefox)
    - [Chrome](#chrome)
  - [Screenshots](#screenshots)
- [Mastodon](#mastodon)
  - [Usage](#usage-3)
  - [Example for reporting via Mastodon](#example-for-reporting-via-mastodon)
    - [Mastodon limitations](#mastodon-limitations)
  - [Change/Remove your Access Token](#changeremove-your-access-token)

------

## Control your issue

You can close, reopen, or rescan your issue created **by you**.
Just write a comment in issue and it will be handled quickly.

You cannot control other user's issue _except_ from [Super users](su.txt).

### All users

| Comment | Result |
| -- | -- |
| `@reporter /close` | Close this issue. |
| `@reporter /reopen` | Reopen this issue. |
| `@reporter /update URL`<br>`@reporter /update` (=`/update this`) | Update the issue description. |


### Super users ONLY

| Comment | Result |
| -- | -- |
| `@reporter /invalid` | Set `Invalid` label & close the issue.<br>(same as `/label Invalid` + `/close`) |
| `@reporter /wontfix` | Set `Wontfix` label & close the issue.<br>(same as `/label Wontfix` + `/close`) |
| `@reporter /label LABEL` | Set `LABEL` label. Comma-separated, Case-Sensitive.<br>e.g. `/label Linked,Tracking` |
| `@reporter /pirated` | Set `Pirated` label.<br>(same to `/label Pirated`) |
| `@reporter /change category URL`<br>`@reporter /change category`<br>(=`/change category this`) | Change the category and update the issue with a new issue format. |
| `@reporter /fp` | [Bot-created Porn-Issue Only]<br>Tell the bot that the site is `not porn` & close the issue as Wontfix<br>**Use with caution - Please read below** |
| `@reporter /ss URL`<br>`@reporter /ss` (=`/ss this`) | Create a new screenshot<br>(_This will take several minutes_) (meaning move on to next issue :smiley:) |
| `@reporter /commit`<br>`@reporter /ok` | Commit this issue |


You can remove the first `/`. For example `@reporter close` is identical
to `@reporter /close`.

If the command is not actionable the bot will add `!` as reaction.


#### About `/update URL`
- The URL's base domain must match the title's domain.
  - e.g. `@reporter /update https://blog.example.com` for updating
    `example.com` issue
- You can also write
    ```
    @reporter /update https://blog.example.com/blog/
    ```
  (The \` will be ignored automatically)

#### About `/change category URL`
- The `category` value: one of `API` -> `cat value` _or_ `pirated`
- For example if you want to resubmit `Porn` issue as `NSFW::Strict`, 
  you will write `@reporter /change pornstrict https://www.example.com`.

#### About `this` URL
- If you are going to update or change current issue _with same URL_, you 
  can use `this` for URL.
  - This does not work with old issues which does not include the URL 
    inside the description.

#### About `/fp` (False Positive) command
- The bot-committed porn issue has `( )`.
  - e.g. First line, `@(BotName) believe this domain is an Adult`
- If you belive the bot reported non-porn site as `porn`, write a comment 
  `@reporter /fp` (or `@reporter fp`).
  - The learning will be done instantly.
- **Important!**
  - 1. Please **do not** write `@reporter fp` on _real_ porn issue. Only 
    report `non-porn` issue.
  - 2. When the `description is very porn` but the screenshot or site is 
    `not porn`, **do not** report as false positive _because_ the bot
    `judge from the description`. **Reporting this will screw up the
    judgement!** (`description is porn` but the user reported it as
    `non-porn`, this is very bad)
    - Just close the issue with wontfix/invalid.

#### About `/ss` (Screenshot) command
- The `/ss` command will deliver the screenshot quickly. There is another
  way to trigger screenshot - add `Screenshot_missing` label to the opened
  issue, and the bot will pick them up every 6 minutes.

-----


## API Token scope

To use this service you need MyPDNS.org API Token. This is for preventing
abusive requests.

The API Token requires `read_user` permission to read `account status`
(banned or not) and  `username on mypdns.org`. Other information such as
your email address is NEVER be read or collected.

There is another _optional_ permission, `api`. If you enable it the bot
will automatically add your vote to already existing issue for
voting-based priority. It is not required although it will help
maintainers to decide their priority.

- Create your personal access token
  - [read_user & api](https://mypdns.org/-/profile/personal_access_tokens?name=mypdnsrep&scopes=read_user,api)
  - [read_user only](https://mypdns.org/-/profile/personal_access_tokens?name=mypdnsrep&scopes=read_user)


## API

Just send a URL and it will turn into useful issue!

Clearnet & Tor:

```shell
curl --http2 -d "k=YOUR-PRIVATE-API-KEY&cat=news&url=https://nytimes.com/" -X POST https://API-URL-HERE
```

Tor:

```shell
curl -x socks5h://127.0.0.1:9050 -d "k=YOUR-PRIVATE-API-KEY&cat=news&url=https://nytimes.com/" -X POST http://API-URL-HERE
```

- This is not my server (shared). It has zero-log policy (no log).
  - http://about-karmaapi.go.crimeflare.eu.org

----


### API URL

| Type | URL |
| -- | -- |
| Clearnet | `https://karma.crimeflare.eu.org:1984/api/mypdns/` |
| [Tor] | `http://karma.im5wixghmfmt7gf7wb4xrgdm6byx2gj26zn47da6nwo7xvybgxnqryid.onion/api/mypdns/` |


### POST data

| required? | name | value |
| --- | --- | --- |
| :heavy_check_mark: | k | **K**ey. read below |
| :heavy_check_mark: | url | Full URL of the website |
| :heavy_check_mark: | cat | Category of the website<br>(the `cat` value _OR_ `in Matrix|Porn` value) |
| :heavy_minus_sign: | wmemo | **Optional** Comment. UTF-16LE BASE64 encoded. |
| :heavy_minus_sign: | wdesc | **Optional** Existence. If set & there is already an issue, the reply value will be the issue's description. |

If you want to send `wmemo` encode it like this `echo "This is my comment." | iconv -f UTF-8 -t UTF-16LE | base64` then run `curl --http2 -d "k=(TOKEN)&url=(URL)&wmemo=(BASE64 Encoded)" -X...`.

### The `cat` value (Non-Porn)

| Category | Case-Sensitive format |
| --- | --- |
| adware | `AdWare` |
| drugs | `Drugs` |
| gambling | `Gambling` |
| malicious | `Malicious` |
| movies | `Movies` |
| news | `News` |
| phishing | `Phishing` |
| politics | `Politics` |
| redirector | `Redirector` |
| religion | `Religion` |
| scamming | `Scamming` |
| spyware | `Spyware` |
| torrent | `Torrent` |
| tracking | `Tracking` |
| typosquatting | `Typo_Squatting` |
| weapons | `Weapons` |
| <h3>Adult only</h3> |
| porn | `NSFW::Porn` |
| porngore | `NSFW::Gore` |
| pornsnuff | `NSFW::Snuff` |
| pornstrict | `NSFW::Strict` |


- Change `url` to the URL such as: `https://porn.com/lesbian.html`
- Change `YOUR-PRIVATE-API-KEY` to your personal access token.
  - e.g. `k=byIe_ns-39cWCQ` (not `k="ab-Cd_E"` nor `k='a_bc-DE'`)
- The return value `{'reply':'roger'}` means it accepted your request.
- After some minutes later your issue will appear on Matrix issues (or 
  not, if the issue is already exist or unable to take a screenshot)
  - If there are many submissions this might take hours. You can know 
    your requests are still in line by sending same request again. API
    will respond "`This domain is still waiting in line.`".
- If you are going to report CSAM (aka Child Porn) website add the word 
  `CSAM` to the comment.
  - The issue will be created with Confidential flag.


---

## CLI tool

### Usage

```shell
mypdnsrep token
    Set or delete your mypdns.org token

mypdnsrep category URL|Domain[| Comment]
    category is "API" cat value
    e.g.
      mypdnsrep redirector https://bitly.com/
      mypdnsrep Phishing https://signup-google.com/
      mypdnsrep porn www.porn.com "This is a porn site."

If you input only domain it will be treated as http://domain.
  mypdnsrep phishing www.google.com
  = mypdnsrep phishing http://www.google.com
```

Sit back and let the script do some working before it will appear
committed.


### Download

#### version `1.0.3.3`

  - [mypdnsrep.exe](https://mypdns.org/infrastructure/mypdns-reporter/-/raw/master/cli/mypdnsrep.exe), for Windows
  - [mypdnsrep.linux](https://mypdns.org/infrastructure/mypdns-reporter/-/raw/master/cli/mypdnsrep.linux), for Linux
  - [mypdnsrep.macos](hhttps://mypdns.org/infrastructure/mypdns-reporter/-/raw/master/cli/mypdnsrep.app), for Mac OS
  - [Source code](cli/source.js), The above source code so you can compile it yourself

```
What is changed from previous?

- URL update
```

##### Linux:

```shell
sudo cp mypdnsrep.linux /usr/local/bin/mypdnsrep && \
    sudo chmod +x /usr/local/bin/mypdnsrep && \
    sudo chown root:root /usr/local/bin/mypdnsrep
```

```terminal
sudo wget -O /usr/local/bin/mypdnsrep https://mypdns.org/infrastructure/mypdns-report/-/raw/master/cli/mypdnsrep.linux && \
    sudo chmod +x /usr/local/bin/mypdnsrep && \
    sudo chown root:root /usr/local/bin/mypdnsrep
```

You might need to reload your `$PATH:` now, or just reopen your terminal.

---

##### GUI tool (Windows)

###### Usage
  - How to use
    - Extract the ZIP file to some folder and run `mypdns.exe`.
  - Uninstall/Remove
    - Just simply delete those files.

##### Config
You can configure this software by editing `mypdns.conf` file.
_Close mypdns.exe before editing this file!_
The config file is like this:

```
50 (the last window position)
50 (the last window position)
xxxxxxx (your token)
#noTop
#useTor
```

- `#noTop`
  - By default the window always stay on top.
  - You can disable this by changing this line to `noTop` (just remove `#`)
- `#useTor`
  - By default the software connect directly (just like above API examples)
  - If you have Tor listening on `127.0.0.1:9050` you can enable this by
    changing it to `useTor`.

#### version `1.0.1`
  - [gui.zip](https://mypdns.org/infrastructure/mypdns-reporter/-/raw/master/gui/gui.zip), for Windows
  - [Source code](gui/source.sp), The above source code so you can compile it yourself

```
What is changed from previous?

- URL update
```

---

## Browser Add-ons

### Usage
  - Right-click the page or link and you will find a report button.
  - You can include small details(memo) or
    [related links](https://mypdns.org/infrastructure/mypdns-reporter/-/issues/77#note_2889168).


### Download Browser Add-on

Current version: `1.0.1`

#### Firefox
  - [Download on Addons for Firefox](https://addons.mozilla.org/en-US/firefox/addon/my-privacy-dns-reporter/)
  - [Download on mypdns.org: addon.xpi](https://mypdns.org/infrastructure/mypdns-reporter/-/raw/master/addon/firefox/addon.xpi)
#### Chrome
  - [Download on mypdns.org: addon.crx](https://mypdns.org/infrastructure/mypdns-reporter/-/raw/master/addon/chrome/addon.crx)
  - Install
    - 1. Download [Ungoogled Chromium](https://github.com/Eloston/ungoogled-chromium)
    - 2. Open Extensions `chrome://extensions/`
    - 3. Enable `Developer Mode`
    - 4. Drag and drop `addon.crx` into the window.
    - 5. `Add "My Privacy DNS Reporter"?` will appear.


```
What is changed from previous?

- URL update
```

### Screenshots

![Menu Drop down with room for adding comment](.assets/screenshot_20220317-5.webp)
![Right cllick menu options](.assets/screenshot_20220317-6.webp)


---

## Mastodon

### Usage

1. Follow https://101010.pl/@mypdns (Used for publishing messages) and send
   something like this.
  - e.g. `@mypdns@101010.pl hello`
    - If your account is new the Bot will ask for your
      [API Access Token][#api-token-scope].
    - You just create a token and reply it.
      - `Direct Message`(`mentioned user only`) is recommended.
2. Send `category`("API" cat value) and `URL`.
   - You can add some comments if necessary, *prefered* by moderators to
     help solving the issue, but **optional**

### Example for reporting via Mastodon

```
@mypdns porn https://porn.com
```

```
@mypdns

NSFW::Gore https://porn.com/gore/
```

```
@mypdns
this is just some comment
spyware https://www.example.com
and another comment
```

```
@mypdns
I found this
spyware https://www.example.com/spyware/
this is related to news,
https://www.news01.com/news/
https://www.news02.net/newstoday/
```

#### Mastodon limitations

> **ONLY**: `One cat-URL per one toot`. 

The Below example will only process `newssite1.com`.

```
@mypdns
news https://newssite1.com/news/
news https://newssite2.com/news/
news https://newssite3.com/news/
```

### Change/Remove your Access Token

- Just send `logout` and it will report back `Your Access Token is cleared.`
  - If you want to report something again, you need to DM your access token again.

```
@mypdns logout
```

[MI]: https://mypdns.org/my-privacy-dns/matrix/-/issues "My Privacy DNS Issues"
